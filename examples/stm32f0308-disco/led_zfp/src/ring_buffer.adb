package body Ring_Buffer is
   procedure Initialize(RB: in out RingBuffer) is
   begin
      RB.Head := 0;
      RB.Tail := 0;
   end;

   procedure Put(RB: in out RingBuffer; C : in Character) is
   begin
      if not Is_Full(RB) then
         RB.Data(RB.Tail) := C;
         RB.Tail := RB.Tail + 1;
         --else we cry
      end if;
   end;

   function Get(RB: in out RingBuffer) return Character is
   begin
      return R : Character do
         R := RB.Data(RB.Head);
         RB.Head := RB.Head + 1;
      end return;
   end;

   function Is_Empty(RB: in RingBuffer) return Boolean is
     (RB.Head = RB.Tail);

   function Is_Full(RB: in RingBuffer) return Boolean is
     (RB.Tail = RB.Head - 1);
end;
