--  This spec has been automatically generated from STM32F030.svd

pragma Restrictions (No_Elaboration_Code);
pragma Ada_2012;
pragma Style_Checks (Off);

with System;

package stm32f030.GPIO is
   pragma Preelaborate;

   ---------------
   -- Registers --
   ---------------

   subtype MODER_MODER0_Field is stm32f030.UInt2;
   subtype MODER_MODER1_Field is stm32f030.UInt2;
   subtype MODER_MODER2_Field is stm32f030.UInt2;
   subtype MODER_MODER3_Field is stm32f030.UInt2;
   subtype MODER_MODER4_Field is stm32f030.UInt2;
   subtype MODER_MODER5_Field is stm32f030.UInt2;
   subtype MODER_MODER6_Field is stm32f030.UInt2;
   subtype MODER_MODER7_Field is stm32f030.UInt2;
   subtype MODER_MODER8_Field is stm32f030.UInt2;
   subtype MODER_MODER9_Field is stm32f030.UInt2;
   subtype MODER_MODER10_Field is stm32f030.UInt2;
   subtype MODER_MODER11_Field is stm32f030.UInt2;
   subtype MODER_MODER12_Field is stm32f030.UInt2;
   subtype MODER_MODER13_Field is stm32f030.UInt2;
   subtype MODER_MODER14_Field is stm32f030.UInt2;
   subtype MODER_MODER15_Field is stm32f030.UInt2;

   --  GPIO port mode register
   type MODER_Register is record
      --  Port x configuration bits (y = 0..15)
      MODER0  : MODER_MODER0_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER1  : MODER_MODER1_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER2  : MODER_MODER2_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER3  : MODER_MODER3_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER4  : MODER_MODER4_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER5  : MODER_MODER5_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER6  : MODER_MODER6_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER7  : MODER_MODER7_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER8  : MODER_MODER8_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER9  : MODER_MODER9_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER10 : MODER_MODER10_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER11 : MODER_MODER11_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER12 : MODER_MODER12_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      MODER13 : MODER_MODER13_Field := 16#2#;
      --  Port x configuration bits (y = 0..15)
      MODER14 : MODER_MODER14_Field := 16#2#;
      --  Port x configuration bits (y = 0..15)
      MODER15 : MODER_MODER15_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for MODER_Register use record
      MODER0  at 0 range 0 .. 1;
      MODER1  at 0 range 2 .. 3;
      MODER2  at 0 range 4 .. 5;
      MODER3  at 0 range 6 .. 7;
      MODER4  at 0 range 8 .. 9;
      MODER5  at 0 range 10 .. 11;
      MODER6  at 0 range 12 .. 13;
      MODER7  at 0 range 14 .. 15;
      MODER8  at 0 range 16 .. 17;
      MODER9  at 0 range 18 .. 19;
      MODER10 at 0 range 20 .. 21;
      MODER11 at 0 range 22 .. 23;
      MODER12 at 0 range 24 .. 25;
      MODER13 at 0 range 26 .. 27;
      MODER14 at 0 range 28 .. 29;
      MODER15 at 0 range 30 .. 31;
   end record;

   subtype OTYPER_OT0_Field is stm32f030.Bit;
   subtype OTYPER_OT1_Field is stm32f030.Bit;
   subtype OTYPER_OT2_Field is stm32f030.Bit;
   subtype OTYPER_OT3_Field is stm32f030.Bit;
   subtype OTYPER_OT4_Field is stm32f030.Bit;
   subtype OTYPER_OT5_Field is stm32f030.Bit;
   subtype OTYPER_OT6_Field is stm32f030.Bit;
   subtype OTYPER_OT7_Field is stm32f030.Bit;
   subtype OTYPER_OT8_Field is stm32f030.Bit;
   subtype OTYPER_OT9_Field is stm32f030.Bit;
   subtype OTYPER_OT10_Field is stm32f030.Bit;
   subtype OTYPER_OT11_Field is stm32f030.Bit;
   subtype OTYPER_OT12_Field is stm32f030.Bit;
   subtype OTYPER_OT13_Field is stm32f030.Bit;
   subtype OTYPER_OT14_Field is stm32f030.Bit;
   subtype OTYPER_OT15_Field is stm32f030.Bit;

   --  GPIO port output type register
   type OTYPER_Register is record
      --  Port x configuration bits (y = 0..15)
      OT0            : OTYPER_OT0_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT1            : OTYPER_OT1_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT2            : OTYPER_OT2_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT3            : OTYPER_OT3_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT4            : OTYPER_OT4_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT5            : OTYPER_OT5_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT6            : OTYPER_OT6_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT7            : OTYPER_OT7_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT8            : OTYPER_OT8_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT9            : OTYPER_OT9_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT10           : OTYPER_OT10_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT11           : OTYPER_OT11_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT12           : OTYPER_OT12_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT13           : OTYPER_OT13_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT14           : OTYPER_OT14_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OT15           : OTYPER_OT15_Field := 16#0#;
      --  unspecified
      Reserved_16_31 : stm32f030.UInt16 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for OTYPER_Register use record
      OT0            at 0 range 0 .. 0;
      OT1            at 0 range 1 .. 1;
      OT2            at 0 range 2 .. 2;
      OT3            at 0 range 3 .. 3;
      OT4            at 0 range 4 .. 4;
      OT5            at 0 range 5 .. 5;
      OT6            at 0 range 6 .. 6;
      OT7            at 0 range 7 .. 7;
      OT8            at 0 range 8 .. 8;
      OT9            at 0 range 9 .. 9;
      OT10           at 0 range 10 .. 10;
      OT11           at 0 range 11 .. 11;
      OT12           at 0 range 12 .. 12;
      OT13           at 0 range 13 .. 13;
      OT14           at 0 range 14 .. 14;
      OT15           at 0 range 15 .. 15;
      Reserved_16_31 at 0 range 16 .. 31;
   end record;

   subtype OSPEEDR_OSPEEDR0_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR1_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR2_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR3_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR4_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR5_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR6_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR7_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR8_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR9_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR10_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR11_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR12_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR13_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR14_Field is stm32f030.UInt2;
   subtype OSPEEDR_OSPEEDR15_Field is stm32f030.UInt2;

   --  GPIO port output speed register
   type OSPEEDR_Register is record
      --  Port x configuration bits (y = 0..15)
      OSPEEDR0  : OSPEEDR_OSPEEDR0_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR1  : OSPEEDR_OSPEEDR1_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR2  : OSPEEDR_OSPEEDR2_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR3  : OSPEEDR_OSPEEDR3_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR4  : OSPEEDR_OSPEEDR4_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR5  : OSPEEDR_OSPEEDR5_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR6  : OSPEEDR_OSPEEDR6_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR7  : OSPEEDR_OSPEEDR7_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR8  : OSPEEDR_OSPEEDR8_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR9  : OSPEEDR_OSPEEDR9_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR10 : OSPEEDR_OSPEEDR10_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR11 : OSPEEDR_OSPEEDR11_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR12 : OSPEEDR_OSPEEDR12_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR13 : OSPEEDR_OSPEEDR13_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR14 : OSPEEDR_OSPEEDR14_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      OSPEEDR15 : OSPEEDR_OSPEEDR15_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for OSPEEDR_Register use record
      OSPEEDR0  at 0 range 0 .. 1;
      OSPEEDR1  at 0 range 2 .. 3;
      OSPEEDR2  at 0 range 4 .. 5;
      OSPEEDR3  at 0 range 6 .. 7;
      OSPEEDR4  at 0 range 8 .. 9;
      OSPEEDR5  at 0 range 10 .. 11;
      OSPEEDR6  at 0 range 12 .. 13;
      OSPEEDR7  at 0 range 14 .. 15;
      OSPEEDR8  at 0 range 16 .. 17;
      OSPEEDR9  at 0 range 18 .. 19;
      OSPEEDR10 at 0 range 20 .. 21;
      OSPEEDR11 at 0 range 22 .. 23;
      OSPEEDR12 at 0 range 24 .. 25;
      OSPEEDR13 at 0 range 26 .. 27;
      OSPEEDR14 at 0 range 28 .. 29;
      OSPEEDR15 at 0 range 30 .. 31;
   end record;

   subtype PUPDR_PUPDR0_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR1_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR2_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR3_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR4_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR5_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR6_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR7_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR8_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR9_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR10_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR11_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR12_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR13_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR14_Field is stm32f030.UInt2;
   subtype PUPDR_PUPDR15_Field is stm32f030.UInt2;

   --  GPIO port pull-up/pull-down register
   type PUPDR_Register is record
      --  Port x configuration bits (y = 0..15)
      PUPDR0  : PUPDR_PUPDR0_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR1  : PUPDR_PUPDR1_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR2  : PUPDR_PUPDR2_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR3  : PUPDR_PUPDR3_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR4  : PUPDR_PUPDR4_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR5  : PUPDR_PUPDR5_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR6  : PUPDR_PUPDR6_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR7  : PUPDR_PUPDR7_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR8  : PUPDR_PUPDR8_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR9  : PUPDR_PUPDR9_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR10 : PUPDR_PUPDR10_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR11 : PUPDR_PUPDR11_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR12 : PUPDR_PUPDR12_Field := 16#0#;
      --  Port x configuration bits (y = 0..15)
      PUPDR13 : PUPDR_PUPDR13_Field := 16#1#;
      --  Port x configuration bits (y = 0..15)
      PUPDR14 : PUPDR_PUPDR14_Field := 16#2#;
      --  Port x configuration bits (y = 0..15)
      PUPDR15 : PUPDR_PUPDR15_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for PUPDR_Register use record
      PUPDR0  at 0 range 0 .. 1;
      PUPDR1  at 0 range 2 .. 3;
      PUPDR2  at 0 range 4 .. 5;
      PUPDR3  at 0 range 6 .. 7;
      PUPDR4  at 0 range 8 .. 9;
      PUPDR5  at 0 range 10 .. 11;
      PUPDR6  at 0 range 12 .. 13;
      PUPDR7  at 0 range 14 .. 15;
      PUPDR8  at 0 range 16 .. 17;
      PUPDR9  at 0 range 18 .. 19;
      PUPDR10 at 0 range 20 .. 21;
      PUPDR11 at 0 range 22 .. 23;
      PUPDR12 at 0 range 24 .. 25;
      PUPDR13 at 0 range 26 .. 27;
      PUPDR14 at 0 range 28 .. 29;
      PUPDR15 at 0 range 30 .. 31;
   end record;

   subtype IDR_IDR0_Field is stm32f030.Bit;
   subtype IDR_IDR1_Field is stm32f030.Bit;
   subtype IDR_IDR2_Field is stm32f030.Bit;
   subtype IDR_IDR3_Field is stm32f030.Bit;
   subtype IDR_IDR4_Field is stm32f030.Bit;
   subtype IDR_IDR5_Field is stm32f030.Bit;
   subtype IDR_IDR6_Field is stm32f030.Bit;
   subtype IDR_IDR7_Field is stm32f030.Bit;
   subtype IDR_IDR8_Field is stm32f030.Bit;
   subtype IDR_IDR9_Field is stm32f030.Bit;
   subtype IDR_IDR10_Field is stm32f030.Bit;
   subtype IDR_IDR11_Field is stm32f030.Bit;
   subtype IDR_IDR12_Field is stm32f030.Bit;
   subtype IDR_IDR13_Field is stm32f030.Bit;
   subtype IDR_IDR14_Field is stm32f030.Bit;
   subtype IDR_IDR15_Field is stm32f030.Bit;

   --  GPIO port input data register
   type IDR_Register is record
      --  Read-only. Port input data (y = 0..15)
      IDR0           : IDR_IDR0_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR1           : IDR_IDR1_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR2           : IDR_IDR2_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR3           : IDR_IDR3_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR4           : IDR_IDR4_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR5           : IDR_IDR5_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR6           : IDR_IDR6_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR7           : IDR_IDR7_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR8           : IDR_IDR8_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR9           : IDR_IDR9_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR10          : IDR_IDR10_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR11          : IDR_IDR11_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR12          : IDR_IDR12_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR13          : IDR_IDR13_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR14          : IDR_IDR14_Field;
      --  Read-only. Port input data (y = 0..15)
      IDR15          : IDR_IDR15_Field;
      --  unspecified
      Reserved_16_31 : stm32f030.UInt16;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for IDR_Register use record
      IDR0           at 0 range 0 .. 0;
      IDR1           at 0 range 1 .. 1;
      IDR2           at 0 range 2 .. 2;
      IDR3           at 0 range 3 .. 3;
      IDR4           at 0 range 4 .. 4;
      IDR5           at 0 range 5 .. 5;
      IDR6           at 0 range 6 .. 6;
      IDR7           at 0 range 7 .. 7;
      IDR8           at 0 range 8 .. 8;
      IDR9           at 0 range 9 .. 9;
      IDR10          at 0 range 10 .. 10;
      IDR11          at 0 range 11 .. 11;
      IDR12          at 0 range 12 .. 12;
      IDR13          at 0 range 13 .. 13;
      IDR14          at 0 range 14 .. 14;
      IDR15          at 0 range 15 .. 15;
      Reserved_16_31 at 0 range 16 .. 31;
   end record;

   subtype ODR_ODR0_Field is stm32f030.Bit;
   subtype ODR_ODR1_Field is stm32f030.Bit;
   subtype ODR_ODR2_Field is stm32f030.Bit;
   subtype ODR_ODR3_Field is stm32f030.Bit;
   subtype ODR_ODR4_Field is stm32f030.Bit;
   subtype ODR_ODR5_Field is stm32f030.Bit;
   subtype ODR_ODR6_Field is stm32f030.Bit;
   subtype ODR_ODR7_Field is stm32f030.Bit;
   subtype ODR_ODR8_Field is stm32f030.Bit;
   subtype ODR_ODR9_Field is stm32f030.Bit;
   subtype ODR_ODR10_Field is stm32f030.Bit;
   subtype ODR_ODR11_Field is stm32f030.Bit;
   subtype ODR_ODR12_Field is stm32f030.Bit;
   subtype ODR_ODR13_Field is stm32f030.Bit;
   subtype ODR_ODR14_Field is stm32f030.Bit;
   subtype ODR_ODR15_Field is stm32f030.Bit;

   --  GPIO port output data register
   type ODR_Register is record
      --  Port output data (y = 0..15)
      ODR0           : ODR_ODR0_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR1           : ODR_ODR1_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR2           : ODR_ODR2_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR3           : ODR_ODR3_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR4           : ODR_ODR4_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR5           : ODR_ODR5_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR6           : ODR_ODR6_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR7           : ODR_ODR7_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR8           : ODR_ODR8_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR9           : ODR_ODR9_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR10          : ODR_ODR10_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR11          : ODR_ODR11_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR12          : ODR_ODR12_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR13          : ODR_ODR13_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR14          : ODR_ODR14_Field := 16#0#;
      --  Port output data (y = 0..15)
      ODR15          : ODR_ODR15_Field := 16#0#;
      --  unspecified
      Reserved_16_31 : stm32f030.UInt16 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for ODR_Register use record
      ODR0           at 0 range 0 .. 0;
      ODR1           at 0 range 1 .. 1;
      ODR2           at 0 range 2 .. 2;
      ODR3           at 0 range 3 .. 3;
      ODR4           at 0 range 4 .. 4;
      ODR5           at 0 range 5 .. 5;
      ODR6           at 0 range 6 .. 6;
      ODR7           at 0 range 7 .. 7;
      ODR8           at 0 range 8 .. 8;
      ODR9           at 0 range 9 .. 9;
      ODR10          at 0 range 10 .. 10;
      ODR11          at 0 range 11 .. 11;
      ODR12          at 0 range 12 .. 12;
      ODR13          at 0 range 13 .. 13;
      ODR14          at 0 range 14 .. 14;
      ODR15          at 0 range 15 .. 15;
      Reserved_16_31 at 0 range 16 .. 31;
   end record;

   subtype BSRR_BS0_Field is stm32f030.Bit;
   subtype BSRR_BS1_Field is stm32f030.Bit;
   subtype BSRR_BS2_Field is stm32f030.Bit;
   subtype BSRR_BS3_Field is stm32f030.Bit;
   subtype BSRR_BS4_Field is stm32f030.Bit;
   subtype BSRR_BS5_Field is stm32f030.Bit;
   subtype BSRR_BS6_Field is stm32f030.Bit;
   subtype BSRR_BS7_Field is stm32f030.Bit;
   subtype BSRR_BS8_Field is stm32f030.Bit;
   subtype BSRR_BS9_Field is stm32f030.Bit;
   subtype BSRR_BS10_Field is stm32f030.Bit;
   subtype BSRR_BS11_Field is stm32f030.Bit;
   subtype BSRR_BS12_Field is stm32f030.Bit;
   subtype BSRR_BS13_Field is stm32f030.Bit;
   subtype BSRR_BS14_Field is stm32f030.Bit;
   subtype BSRR_BS15_Field is stm32f030.Bit;
   subtype BSRR_BR0_Field is stm32f030.Bit;
   subtype BSRR_BR1_Field is stm32f030.Bit;
   subtype BSRR_BR2_Field is stm32f030.Bit;
   subtype BSRR_BR3_Field is stm32f030.Bit;
   subtype BSRR_BR4_Field is stm32f030.Bit;
   subtype BSRR_BR5_Field is stm32f030.Bit;
   subtype BSRR_BR6_Field is stm32f030.Bit;
   subtype BSRR_BR7_Field is stm32f030.Bit;
   subtype BSRR_BR8_Field is stm32f030.Bit;
   subtype BSRR_BR9_Field is stm32f030.Bit;
   subtype BSRR_BR10_Field is stm32f030.Bit;
   subtype BSRR_BR11_Field is stm32f030.Bit;
   subtype BSRR_BR12_Field is stm32f030.Bit;
   subtype BSRR_BR13_Field is stm32f030.Bit;
   subtype BSRR_BR14_Field is stm32f030.Bit;
   subtype BSRR_BR15_Field is stm32f030.Bit;

   --  GPIO port bit set/reset register
   type BSRR_Register is record
      --  Write-only. Port x set bit y (y= 0..15)
      BS0  : BSRR_BS0_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS1  : BSRR_BS1_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS2  : BSRR_BS2_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS3  : BSRR_BS3_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS4  : BSRR_BS4_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS5  : BSRR_BS5_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS6  : BSRR_BS6_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS7  : BSRR_BS7_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS8  : BSRR_BS8_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS9  : BSRR_BS9_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS10 : BSRR_BS10_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS11 : BSRR_BS11_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS12 : BSRR_BS12_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS13 : BSRR_BS13_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS14 : BSRR_BS14_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BS15 : BSRR_BS15_Field := 16#0#;
      --  Write-only. Port x set bit y (y= 0..15)
      BR0  : BSRR_BR0_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR1  : BSRR_BR1_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR2  : BSRR_BR2_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR3  : BSRR_BR3_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR4  : BSRR_BR4_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR5  : BSRR_BR5_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR6  : BSRR_BR6_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR7  : BSRR_BR7_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR8  : BSRR_BR8_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR9  : BSRR_BR9_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR10 : BSRR_BR10_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR11 : BSRR_BR11_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR12 : BSRR_BR12_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR13 : BSRR_BR13_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR14 : BSRR_BR14_Field := 16#0#;
      --  Write-only. Port x reset bit y (y = 0..15)
      BR15 : BSRR_BR15_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for BSRR_Register use record
      BS0  at 0 range 0 .. 0;
      BS1  at 0 range 1 .. 1;
      BS2  at 0 range 2 .. 2;
      BS3  at 0 range 3 .. 3;
      BS4  at 0 range 4 .. 4;
      BS5  at 0 range 5 .. 5;
      BS6  at 0 range 6 .. 6;
      BS7  at 0 range 7 .. 7;
      BS8  at 0 range 8 .. 8;
      BS9  at 0 range 9 .. 9;
      BS10 at 0 range 10 .. 10;
      BS11 at 0 range 11 .. 11;
      BS12 at 0 range 12 .. 12;
      BS13 at 0 range 13 .. 13;
      BS14 at 0 range 14 .. 14;
      BS15 at 0 range 15 .. 15;
      BR0  at 0 range 16 .. 16;
      BR1  at 0 range 17 .. 17;
      BR2  at 0 range 18 .. 18;
      BR3  at 0 range 19 .. 19;
      BR4  at 0 range 20 .. 20;
      BR5  at 0 range 21 .. 21;
      BR6  at 0 range 22 .. 22;
      BR7  at 0 range 23 .. 23;
      BR8  at 0 range 24 .. 24;
      BR9  at 0 range 25 .. 25;
      BR10 at 0 range 26 .. 26;
      BR11 at 0 range 27 .. 27;
      BR12 at 0 range 28 .. 28;
      BR13 at 0 range 29 .. 29;
      BR14 at 0 range 30 .. 30;
      BR15 at 0 range 31 .. 31;
   end record;

   subtype LCKR_LCK0_Field is stm32f030.Bit;
   subtype LCKR_LCK1_Field is stm32f030.Bit;
   subtype LCKR_LCK2_Field is stm32f030.Bit;
   subtype LCKR_LCK3_Field is stm32f030.Bit;
   subtype LCKR_LCK4_Field is stm32f030.Bit;
   subtype LCKR_LCK5_Field is stm32f030.Bit;
   subtype LCKR_LCK6_Field is stm32f030.Bit;
   subtype LCKR_LCK7_Field is stm32f030.Bit;
   subtype LCKR_LCK8_Field is stm32f030.Bit;
   subtype LCKR_LCK9_Field is stm32f030.Bit;
   subtype LCKR_LCK10_Field is stm32f030.Bit;
   subtype LCKR_LCK11_Field is stm32f030.Bit;
   subtype LCKR_LCK12_Field is stm32f030.Bit;
   subtype LCKR_LCK13_Field is stm32f030.Bit;
   subtype LCKR_LCK14_Field is stm32f030.Bit;
   subtype LCKR_LCK15_Field is stm32f030.Bit;
   subtype LCKR_LCKK_Field is stm32f030.Bit;

   --  GPIO port configuration lock register
   type LCKR_Register is record
      --  Port x lock bit y (y= 0..15)
      LCK0           : LCKR_LCK0_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK1           : LCKR_LCK1_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK2           : LCKR_LCK2_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK3           : LCKR_LCK3_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK4           : LCKR_LCK4_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK5           : LCKR_LCK5_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK6           : LCKR_LCK6_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK7           : LCKR_LCK7_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK8           : LCKR_LCK8_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK9           : LCKR_LCK9_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK10          : LCKR_LCK10_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK11          : LCKR_LCK11_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK12          : LCKR_LCK12_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK13          : LCKR_LCK13_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK14          : LCKR_LCK14_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCK15          : LCKR_LCK15_Field := 16#0#;
      --  Port x lock bit y (y= 0..15)
      LCKK           : LCKR_LCKK_Field := 16#0#;
      --  unspecified
      Reserved_17_31 : stm32f030.UInt15 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for LCKR_Register use record
      LCK0           at 0 range 0 .. 0;
      LCK1           at 0 range 1 .. 1;
      LCK2           at 0 range 2 .. 2;
      LCK3           at 0 range 3 .. 3;
      LCK4           at 0 range 4 .. 4;
      LCK5           at 0 range 5 .. 5;
      LCK6           at 0 range 6 .. 6;
      LCK7           at 0 range 7 .. 7;
      LCK8           at 0 range 8 .. 8;
      LCK9           at 0 range 9 .. 9;
      LCK10          at 0 range 10 .. 10;
      LCK11          at 0 range 11 .. 11;
      LCK12          at 0 range 12 .. 12;
      LCK13          at 0 range 13 .. 13;
      LCK14          at 0 range 14 .. 14;
      LCK15          at 0 range 15 .. 15;
      LCKK           at 0 range 16 .. 16;
      Reserved_17_31 at 0 range 17 .. 31;
   end record;

   subtype AFRL_AFRL0_Field is stm32f030.UInt4;
   subtype AFRL_AFRL1_Field is stm32f030.UInt4;
   subtype AFRL_AFRL2_Field is stm32f030.UInt4;
   subtype AFRL_AFRL3_Field is stm32f030.UInt4;
   subtype AFRL_AFRL4_Field is stm32f030.UInt4;
   subtype AFRL_AFRL5_Field is stm32f030.UInt4;
   subtype AFRL_AFRL6_Field is stm32f030.UInt4;
   subtype AFRL_AFRL7_Field is stm32f030.UInt4;

   --  GPIO alternate function low register
   type AFRL_Register is record
      --  Alternate function selection for port x bit y (y = 0..7)
      AFRL0 : AFRL_AFRL0_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 0..7)
      AFRL1 : AFRL_AFRL1_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 0..7)
      AFRL2 : AFRL_AFRL2_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 0..7)
      AFRL3 : AFRL_AFRL3_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 0..7)
      AFRL4 : AFRL_AFRL4_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 0..7)
      AFRL5 : AFRL_AFRL5_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 0..7)
      AFRL6 : AFRL_AFRL6_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 0..7)
      AFRL7 : AFRL_AFRL7_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for AFRL_Register use record
      AFRL0 at 0 range 0 .. 3;
      AFRL1 at 0 range 4 .. 7;
      AFRL2 at 0 range 8 .. 11;
      AFRL3 at 0 range 12 .. 15;
      AFRL4 at 0 range 16 .. 19;
      AFRL5 at 0 range 20 .. 23;
      AFRL6 at 0 range 24 .. 27;
      AFRL7 at 0 range 28 .. 31;
   end record;

   subtype AFRH_AFRH8_Field is stm32f030.UInt4;
   subtype AFRH_AFRH9_Field is stm32f030.UInt4;
   subtype AFRH_AFRH10_Field is stm32f030.UInt4;
   subtype AFRH_AFRH11_Field is stm32f030.UInt4;
   subtype AFRH_AFRH12_Field is stm32f030.UInt4;
   subtype AFRH_AFRH13_Field is stm32f030.UInt4;
   subtype AFRH_AFRH14_Field is stm32f030.UInt4;
   subtype AFRH_AFRH15_Field is stm32f030.UInt4;

   --  GPIO alternate function high register
   type AFRH_Register is record
      --  Alternate function selection for port x bit y (y = 8..15)
      AFRH8  : AFRH_AFRH8_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 8..15)
      AFRH9  : AFRH_AFRH9_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 8..15)
      AFRH10 : AFRH_AFRH10_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 8..15)
      AFRH11 : AFRH_AFRH11_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 8..15)
      AFRH12 : AFRH_AFRH12_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 8..15)
      AFRH13 : AFRH_AFRH13_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 8..15)
      AFRH14 : AFRH_AFRH14_Field := 16#0#;
      --  Alternate function selection for port x bit y (y = 8..15)
      AFRH15 : AFRH_AFRH15_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for AFRH_Register use record
      AFRH8  at 0 range 0 .. 3;
      AFRH9  at 0 range 4 .. 7;
      AFRH10 at 0 range 8 .. 11;
      AFRH11 at 0 range 12 .. 15;
      AFRH12 at 0 range 16 .. 19;
      AFRH13 at 0 range 20 .. 23;
      AFRH14 at 0 range 24 .. 27;
      AFRH15 at 0 range 28 .. 31;
   end record;

   subtype BRR_BR0_Field is stm32f030.Bit;
   subtype BRR_BR1_Field is stm32f030.Bit;
   subtype BRR_BR2_Field is stm32f030.Bit;
   subtype BRR_BR3_Field is stm32f030.Bit;
   subtype BRR_BR4_Field is stm32f030.Bit;
   subtype BRR_BR5_Field is stm32f030.Bit;
   subtype BRR_BR6_Field is stm32f030.Bit;
   subtype BRR_BR7_Field is stm32f030.Bit;
   subtype BRR_BR8_Field is stm32f030.Bit;
   subtype BRR_BR9_Field is stm32f030.Bit;
   subtype BRR_BR10_Field is stm32f030.Bit;
   subtype BRR_BR11_Field is stm32f030.Bit;
   subtype BRR_BR12_Field is stm32f030.Bit;
   subtype BRR_BR13_Field is stm32f030.Bit;
   subtype BRR_BR14_Field is stm32f030.Bit;
   subtype BRR_BR15_Field is stm32f030.Bit;

   --  Port bit reset register
   type BRR_Register is record
      --  Write-only. Port x Reset bit y
      BR0            : BRR_BR0_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR1            : BRR_BR1_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR2            : BRR_BR2_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR3            : BRR_BR3_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR4            : BRR_BR4_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR5            : BRR_BR5_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR6            : BRR_BR6_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR7            : BRR_BR7_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR8            : BRR_BR8_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR9            : BRR_BR9_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR10           : BRR_BR10_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR11           : BRR_BR11_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR12           : BRR_BR12_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR13           : BRR_BR13_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR14           : BRR_BR14_Field := 16#0#;
      --  Write-only. Port x Reset bit y
      BR15           : BRR_BR15_Field := 16#0#;
      --  unspecified
      Reserved_16_31 : stm32f030.UInt16 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for BRR_Register use record
      BR0            at 0 range 0 .. 0;
      BR1            at 0 range 1 .. 1;
      BR2            at 0 range 2 .. 2;
      BR3            at 0 range 3 .. 3;
      BR4            at 0 range 4 .. 4;
      BR5            at 0 range 5 .. 5;
      BR6            at 0 range 6 .. 6;
      BR7            at 0 range 7 .. 7;
      BR8            at 0 range 8 .. 8;
      BR9            at 0 range 9 .. 9;
      BR10           at 0 range 10 .. 10;
      BR11           at 0 range 11 .. 11;
      BR12           at 0 range 12 .. 12;
      BR13           at 0 range 13 .. 13;
      BR14           at 0 range 14 .. 14;
      BR15           at 0 range 15 .. 15;
      Reserved_16_31 at 0 range 16 .. 31;
   end record;

   -----------------
   -- Peripherals --
   -----------------

   --  General-purpose I/Os
   type GPIO_Peripheral is record
      --  GPIO port mode register
      MODER0   : aliased MODER_Register;
      --  GPIO port output type register
      OTYPER0  : aliased OTYPER_Register;
      --  GPIO port output speed register
      OSPEEDR0 : aliased OSPEEDR_Register;
      --  GPIO port pull-up/pull-down register
      PUPDR0   : aliased PUPDR_Register;
      --  GPIO port input data register
      IDR0     : aliased IDR_Register;
      --  GPIO port output data register
      ODR0     : aliased ODR_Register;
      --  GPIO port bit set/reset register
      BSRR0    : aliased BSRR_Register;
      --  GPIO port configuration lock register
      LCKR0    : aliased LCKR_Register;
      --  GPIO alternate function low register
      AFRL0    : aliased AFRL_Register;
      --  GPIO alternate function high register
      AFRH0    : aliased AFRH_Register;
      --  Port bit reset register
      BRR0     : aliased BRR_Register;
   end record
     with Volatile;

   for GPIO_Peripheral use record
      MODER0   at 16#0# range 0 .. 31;
      OTYPER0  at 16#4# range 0 .. 31;
      OSPEEDR0 at 16#8# range 0 .. 31;
      PUPDR0   at 16#C# range 0 .. 31;
      IDR0     at 16#10# range 0 .. 31;
      ODR0     at 16#14# range 0 .. 31;
      BSRR0    at 16#18# range 0 .. 31;
      LCKR0    at 16#1C# range 0 .. 31;
      AFRL0    at 16#20# range 0 .. 31;
      AFRH0    at 16#24# range 0 .. 31;
      BRR0     at 16#28# range 0 .. 31;
   end record;

   --  General-purpose I/Os
   GPIOA_Periph : aliased GPIO_Peripheral
     with Import, Address => GPIOA_Base;

   --  General-purpose I/Os
   GPIOB_Periph : aliased GPIO_Peripheral
     with Import, Address => GPIOB_Base;

   --  General-purpose I/Os
   GPIOC_Periph : aliased GPIO_Peripheral
     with Import, Address => GPIOC_Base;

   --  General-purpose I/Os
   GPIOD_Periph : aliased GPIO_Peripheral
     with Import, Address => GPIOD_Base;

   --  General-purpose I/Os
   GPIOF_Periph : aliased GPIO_Peripheral
     with Import, Address => GPIOF_Base;

end stm32f030.GPIO;
