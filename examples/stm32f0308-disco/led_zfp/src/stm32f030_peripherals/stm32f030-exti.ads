--  This spec has been automatically generated from STM32F030.svd

pragma Restrictions (No_Elaboration_Code);
pragma Ada_2012;
pragma Style_Checks (Off);

with System;

package stm32f030.EXTI is
   pragma Preelaborate;

   ---------------
   -- Registers --
   ---------------

   subtype IMR_MR0_Field is stm32f030.Bit;
   subtype IMR_MR1_Field is stm32f030.Bit;
   subtype IMR_MR2_Field is stm32f030.Bit;
   subtype IMR_MR3_Field is stm32f030.Bit;
   subtype IMR_MR4_Field is stm32f030.Bit;
   subtype IMR_MR5_Field is stm32f030.Bit;
   subtype IMR_MR6_Field is stm32f030.Bit;
   subtype IMR_MR7_Field is stm32f030.Bit;
   subtype IMR_MR8_Field is stm32f030.Bit;
   subtype IMR_MR9_Field is stm32f030.Bit;
   subtype IMR_MR10_Field is stm32f030.Bit;
   subtype IMR_MR11_Field is stm32f030.Bit;
   subtype IMR_MR12_Field is stm32f030.Bit;
   subtype IMR_MR13_Field is stm32f030.Bit;
   subtype IMR_MR14_Field is stm32f030.Bit;
   subtype IMR_MR15_Field is stm32f030.Bit;
   subtype IMR_MR16_Field is stm32f030.Bit;
   subtype IMR_MR17_Field is stm32f030.Bit;
   subtype IMR_MR18_Field is stm32f030.Bit;
   subtype IMR_MR19_Field is stm32f030.Bit;
   subtype IMR_MR20_Field is stm32f030.Bit;
   subtype IMR_MR21_Field is stm32f030.Bit;
   subtype IMR_MR22_Field is stm32f030.Bit;
   subtype IMR_MR23_Field is stm32f030.Bit;
   subtype IMR_MR24_Field is stm32f030.Bit;
   subtype IMR_MR25_Field is stm32f030.Bit;
   subtype IMR_MR26_Field is stm32f030.Bit;
   subtype IMR_MR27_Field is stm32f030.Bit;

   --  Interrupt mask register (EXTI_IMR)
   type IMR_Register is record
      --  Interrupt Mask on line 0
      MR0            : IMR_MR0_Field := 16#0#;
      --  Interrupt Mask on line 1
      MR1            : IMR_MR1_Field := 16#0#;
      --  Interrupt Mask on line 2
      MR2            : IMR_MR2_Field := 16#0#;
      --  Interrupt Mask on line 3
      MR3            : IMR_MR3_Field := 16#0#;
      --  Interrupt Mask on line 4
      MR4            : IMR_MR4_Field := 16#0#;
      --  Interrupt Mask on line 5
      MR5            : IMR_MR5_Field := 16#0#;
      --  Interrupt Mask on line 6
      MR6            : IMR_MR6_Field := 16#0#;
      --  Interrupt Mask on line 7
      MR7            : IMR_MR7_Field := 16#0#;
      --  Interrupt Mask on line 8
      MR8            : IMR_MR8_Field := 16#0#;
      --  Interrupt Mask on line 9
      MR9            : IMR_MR9_Field := 16#0#;
      --  Interrupt Mask on line 10
      MR10           : IMR_MR10_Field := 16#0#;
      --  Interrupt Mask on line 11
      MR11           : IMR_MR11_Field := 16#0#;
      --  Interrupt Mask on line 12
      MR12           : IMR_MR12_Field := 16#0#;
      --  Interrupt Mask on line 13
      MR13           : IMR_MR13_Field := 16#0#;
      --  Interrupt Mask on line 14
      MR14           : IMR_MR14_Field := 16#0#;
      --  Interrupt Mask on line 15
      MR15           : IMR_MR15_Field := 16#0#;
      --  Interrupt Mask on line 16
      MR16           : IMR_MR16_Field := 16#0#;
      --  Interrupt Mask on line 17
      MR17           : IMR_MR17_Field := 16#0#;
      --  Interrupt Mask on line 18
      MR18           : IMR_MR18_Field := 16#1#;
      --  Interrupt Mask on line 19
      MR19           : IMR_MR19_Field := 16#0#;
      --  Interrupt Mask on line 20
      MR20           : IMR_MR20_Field := 16#1#;
      --  Interrupt Mask on line 21
      MR21           : IMR_MR21_Field := 16#0#;
      --  Interrupt Mask on line 22
      MR22           : IMR_MR22_Field := 16#0#;
      --  Interrupt Mask on line 23
      MR23           : IMR_MR23_Field := 16#1#;
      --  Interrupt Mask on line 24
      MR24           : IMR_MR24_Field := 16#1#;
      --  Interrupt Mask on line 25
      MR25           : IMR_MR25_Field := 16#1#;
      --  Interrupt Mask on line 26
      MR26           : IMR_MR26_Field := 16#1#;
      --  Interrupt Mask on line 27
      MR27           : IMR_MR27_Field := 16#1#;
      --  unspecified
      Reserved_28_31 : stm32f030.UInt4 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for IMR_Register use record
      MR0            at 0 range 0 .. 0;
      MR1            at 0 range 1 .. 1;
      MR2            at 0 range 2 .. 2;
      MR3            at 0 range 3 .. 3;
      MR4            at 0 range 4 .. 4;
      MR5            at 0 range 5 .. 5;
      MR6            at 0 range 6 .. 6;
      MR7            at 0 range 7 .. 7;
      MR8            at 0 range 8 .. 8;
      MR9            at 0 range 9 .. 9;
      MR10           at 0 range 10 .. 10;
      MR11           at 0 range 11 .. 11;
      MR12           at 0 range 12 .. 12;
      MR13           at 0 range 13 .. 13;
      MR14           at 0 range 14 .. 14;
      MR15           at 0 range 15 .. 15;
      MR16           at 0 range 16 .. 16;
      MR17           at 0 range 17 .. 17;
      MR18           at 0 range 18 .. 18;
      MR19           at 0 range 19 .. 19;
      MR20           at 0 range 20 .. 20;
      MR21           at 0 range 21 .. 21;
      MR22           at 0 range 22 .. 22;
      MR23           at 0 range 23 .. 23;
      MR24           at 0 range 24 .. 24;
      MR25           at 0 range 25 .. 25;
      MR26           at 0 range 26 .. 26;
      MR27           at 0 range 27 .. 27;
      Reserved_28_31 at 0 range 28 .. 31;
   end record;

   subtype EMR_MR0_Field is stm32f030.Bit;
   subtype EMR_MR1_Field is stm32f030.Bit;
   subtype EMR_MR2_Field is stm32f030.Bit;
   subtype EMR_MR3_Field is stm32f030.Bit;
   subtype EMR_MR4_Field is stm32f030.Bit;
   subtype EMR_MR5_Field is stm32f030.Bit;
   subtype EMR_MR6_Field is stm32f030.Bit;
   subtype EMR_MR7_Field is stm32f030.Bit;
   subtype EMR_MR8_Field is stm32f030.Bit;
   subtype EMR_MR9_Field is stm32f030.Bit;
   subtype EMR_MR10_Field is stm32f030.Bit;
   subtype EMR_MR11_Field is stm32f030.Bit;
   subtype EMR_MR12_Field is stm32f030.Bit;
   subtype EMR_MR13_Field is stm32f030.Bit;
   subtype EMR_MR14_Field is stm32f030.Bit;
   subtype EMR_MR15_Field is stm32f030.Bit;
   subtype EMR_MR16_Field is stm32f030.Bit;
   subtype EMR_MR17_Field is stm32f030.Bit;
   subtype EMR_MR18_Field is stm32f030.Bit;
   subtype EMR_MR19_Field is stm32f030.Bit;
   subtype EMR_MR20_Field is stm32f030.Bit;
   subtype EMR_MR21_Field is stm32f030.Bit;
   subtype EMR_MR22_Field is stm32f030.Bit;
   subtype EMR_MR23_Field is stm32f030.Bit;
   subtype EMR_MR24_Field is stm32f030.Bit;
   subtype EMR_MR25_Field is stm32f030.Bit;
   subtype EMR_MR26_Field is stm32f030.Bit;
   subtype EMR_MR27_Field is stm32f030.Bit;

   --  Event mask register (EXTI_EMR)
   type EMR_Register is record
      --  Event Mask on line 0
      MR0            : EMR_MR0_Field := 16#0#;
      --  Event Mask on line 1
      MR1            : EMR_MR1_Field := 16#0#;
      --  Event Mask on line 2
      MR2            : EMR_MR2_Field := 16#0#;
      --  Event Mask on line 3
      MR3            : EMR_MR3_Field := 16#0#;
      --  Event Mask on line 4
      MR4            : EMR_MR4_Field := 16#0#;
      --  Event Mask on line 5
      MR5            : EMR_MR5_Field := 16#0#;
      --  Event Mask on line 6
      MR6            : EMR_MR6_Field := 16#0#;
      --  Event Mask on line 7
      MR7            : EMR_MR7_Field := 16#0#;
      --  Event Mask on line 8
      MR8            : EMR_MR8_Field := 16#0#;
      --  Event Mask on line 9
      MR9            : EMR_MR9_Field := 16#0#;
      --  Event Mask on line 10
      MR10           : EMR_MR10_Field := 16#0#;
      --  Event Mask on line 11
      MR11           : EMR_MR11_Field := 16#0#;
      --  Event Mask on line 12
      MR12           : EMR_MR12_Field := 16#0#;
      --  Event Mask on line 13
      MR13           : EMR_MR13_Field := 16#0#;
      --  Event Mask on line 14
      MR14           : EMR_MR14_Field := 16#0#;
      --  Event Mask on line 15
      MR15           : EMR_MR15_Field := 16#0#;
      --  Event Mask on line 16
      MR16           : EMR_MR16_Field := 16#0#;
      --  Event Mask on line 17
      MR17           : EMR_MR17_Field := 16#0#;
      --  Event Mask on line 18
      MR18           : EMR_MR18_Field := 16#0#;
      --  Event Mask on line 19
      MR19           : EMR_MR19_Field := 16#0#;
      --  Event Mask on line 20
      MR20           : EMR_MR20_Field := 16#0#;
      --  Event Mask on line 21
      MR21           : EMR_MR21_Field := 16#0#;
      --  Event Mask on line 22
      MR22           : EMR_MR22_Field := 16#0#;
      --  Event Mask on line 23
      MR23           : EMR_MR23_Field := 16#0#;
      --  Event Mask on line 24
      MR24           : EMR_MR24_Field := 16#0#;
      --  Event Mask on line 25
      MR25           : EMR_MR25_Field := 16#0#;
      --  Event Mask on line 26
      MR26           : EMR_MR26_Field := 16#0#;
      --  Event Mask on line 27
      MR27           : EMR_MR27_Field := 16#0#;
      --  unspecified
      Reserved_28_31 : stm32f030.UInt4 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for EMR_Register use record
      MR0            at 0 range 0 .. 0;
      MR1            at 0 range 1 .. 1;
      MR2            at 0 range 2 .. 2;
      MR3            at 0 range 3 .. 3;
      MR4            at 0 range 4 .. 4;
      MR5            at 0 range 5 .. 5;
      MR6            at 0 range 6 .. 6;
      MR7            at 0 range 7 .. 7;
      MR8            at 0 range 8 .. 8;
      MR9            at 0 range 9 .. 9;
      MR10           at 0 range 10 .. 10;
      MR11           at 0 range 11 .. 11;
      MR12           at 0 range 12 .. 12;
      MR13           at 0 range 13 .. 13;
      MR14           at 0 range 14 .. 14;
      MR15           at 0 range 15 .. 15;
      MR16           at 0 range 16 .. 16;
      MR17           at 0 range 17 .. 17;
      MR18           at 0 range 18 .. 18;
      MR19           at 0 range 19 .. 19;
      MR20           at 0 range 20 .. 20;
      MR21           at 0 range 21 .. 21;
      MR22           at 0 range 22 .. 22;
      MR23           at 0 range 23 .. 23;
      MR24           at 0 range 24 .. 24;
      MR25           at 0 range 25 .. 25;
      MR26           at 0 range 26 .. 26;
      MR27           at 0 range 27 .. 27;
      Reserved_28_31 at 0 range 28 .. 31;
   end record;

   subtype RTSR_TR0_Field is stm32f030.Bit;
   subtype RTSR_TR1_Field is stm32f030.Bit;
   subtype RTSR_TR2_Field is stm32f030.Bit;
   subtype RTSR_TR3_Field is stm32f030.Bit;
   subtype RTSR_TR4_Field is stm32f030.Bit;
   subtype RTSR_TR5_Field is stm32f030.Bit;
   subtype RTSR_TR6_Field is stm32f030.Bit;
   subtype RTSR_TR7_Field is stm32f030.Bit;
   subtype RTSR_TR8_Field is stm32f030.Bit;
   subtype RTSR_TR9_Field is stm32f030.Bit;
   subtype RTSR_TR10_Field is stm32f030.Bit;
   subtype RTSR_TR11_Field is stm32f030.Bit;
   subtype RTSR_TR12_Field is stm32f030.Bit;
   subtype RTSR_TR13_Field is stm32f030.Bit;
   subtype RTSR_TR14_Field is stm32f030.Bit;
   subtype RTSR_TR15_Field is stm32f030.Bit;
   subtype RTSR_TR16_Field is stm32f030.Bit;
   subtype RTSR_TR17_Field is stm32f030.Bit;
   subtype RTSR_TR19_Field is stm32f030.Bit;

   --  Rising Trigger selection register (EXTI_RTSR)
   type RTSR_Register is record
      --  Rising trigger event configuration of line 0
      TR0            : RTSR_TR0_Field := 16#0#;
      --  Rising trigger event configuration of line 1
      TR1            : RTSR_TR1_Field := 16#0#;
      --  Rising trigger event configuration of line 2
      TR2            : RTSR_TR2_Field := 16#0#;
      --  Rising trigger event configuration of line 3
      TR3            : RTSR_TR3_Field := 16#0#;
      --  Rising trigger event configuration of line 4
      TR4            : RTSR_TR4_Field := 16#0#;
      --  Rising trigger event configuration of line 5
      TR5            : RTSR_TR5_Field := 16#0#;
      --  Rising trigger event configuration of line 6
      TR6            : RTSR_TR6_Field := 16#0#;
      --  Rising trigger event configuration of line 7
      TR7            : RTSR_TR7_Field := 16#0#;
      --  Rising trigger event configuration of line 8
      TR8            : RTSR_TR8_Field := 16#0#;
      --  Rising trigger event configuration of line 9
      TR9            : RTSR_TR9_Field := 16#0#;
      --  Rising trigger event configuration of line 10
      TR10           : RTSR_TR10_Field := 16#0#;
      --  Rising trigger event configuration of line 11
      TR11           : RTSR_TR11_Field := 16#0#;
      --  Rising trigger event configuration of line 12
      TR12           : RTSR_TR12_Field := 16#0#;
      --  Rising trigger event configuration of line 13
      TR13           : RTSR_TR13_Field := 16#0#;
      --  Rising trigger event configuration of line 14
      TR14           : RTSR_TR14_Field := 16#0#;
      --  Rising trigger event configuration of line 15
      TR15           : RTSR_TR15_Field := 16#0#;
      --  Rising trigger event configuration of line 16
      TR16           : RTSR_TR16_Field := 16#0#;
      --  Rising trigger event configuration of line 17
      TR17           : RTSR_TR17_Field := 16#0#;
      --  unspecified
      Reserved_18_18 : stm32f030.Bit := 16#0#;
      --  Rising trigger event configuration of line 19
      TR19           : RTSR_TR19_Field := 16#0#;
      --  unspecified
      Reserved_20_31 : stm32f030.UInt12 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for RTSR_Register use record
      TR0            at 0 range 0 .. 0;
      TR1            at 0 range 1 .. 1;
      TR2            at 0 range 2 .. 2;
      TR3            at 0 range 3 .. 3;
      TR4            at 0 range 4 .. 4;
      TR5            at 0 range 5 .. 5;
      TR6            at 0 range 6 .. 6;
      TR7            at 0 range 7 .. 7;
      TR8            at 0 range 8 .. 8;
      TR9            at 0 range 9 .. 9;
      TR10           at 0 range 10 .. 10;
      TR11           at 0 range 11 .. 11;
      TR12           at 0 range 12 .. 12;
      TR13           at 0 range 13 .. 13;
      TR14           at 0 range 14 .. 14;
      TR15           at 0 range 15 .. 15;
      TR16           at 0 range 16 .. 16;
      TR17           at 0 range 17 .. 17;
      Reserved_18_18 at 0 range 18 .. 18;
      TR19           at 0 range 19 .. 19;
      Reserved_20_31 at 0 range 20 .. 31;
   end record;

   subtype FTSR_TR0_Field is stm32f030.Bit;
   subtype FTSR_TR1_Field is stm32f030.Bit;
   subtype FTSR_TR2_Field is stm32f030.Bit;
   subtype FTSR_TR3_Field is stm32f030.Bit;
   subtype FTSR_TR4_Field is stm32f030.Bit;
   subtype FTSR_TR5_Field is stm32f030.Bit;
   subtype FTSR_TR6_Field is stm32f030.Bit;
   subtype FTSR_TR7_Field is stm32f030.Bit;
   subtype FTSR_TR8_Field is stm32f030.Bit;
   subtype FTSR_TR9_Field is stm32f030.Bit;
   subtype FTSR_TR10_Field is stm32f030.Bit;
   subtype FTSR_TR11_Field is stm32f030.Bit;
   subtype FTSR_TR12_Field is stm32f030.Bit;
   subtype FTSR_TR13_Field is stm32f030.Bit;
   subtype FTSR_TR14_Field is stm32f030.Bit;
   subtype FTSR_TR15_Field is stm32f030.Bit;
   subtype FTSR_TR16_Field is stm32f030.Bit;
   subtype FTSR_TR17_Field is stm32f030.Bit;
   subtype FTSR_TR19_Field is stm32f030.Bit;

   --  Falling Trigger selection register (EXTI_FTSR)
   type FTSR_Register is record
      --  Falling trigger event configuration of line 0
      TR0            : FTSR_TR0_Field := 16#0#;
      --  Falling trigger event configuration of line 1
      TR1            : FTSR_TR1_Field := 16#0#;
      --  Falling trigger event configuration of line 2
      TR2            : FTSR_TR2_Field := 16#0#;
      --  Falling trigger event configuration of line 3
      TR3            : FTSR_TR3_Field := 16#0#;
      --  Falling trigger event configuration of line 4
      TR4            : FTSR_TR4_Field := 16#0#;
      --  Falling trigger event configuration of line 5
      TR5            : FTSR_TR5_Field := 16#0#;
      --  Falling trigger event configuration of line 6
      TR6            : FTSR_TR6_Field := 16#0#;
      --  Falling trigger event configuration of line 7
      TR7            : FTSR_TR7_Field := 16#0#;
      --  Falling trigger event configuration of line 8
      TR8            : FTSR_TR8_Field := 16#0#;
      --  Falling trigger event configuration of line 9
      TR9            : FTSR_TR9_Field := 16#0#;
      --  Falling trigger event configuration of line 10
      TR10           : FTSR_TR10_Field := 16#0#;
      --  Falling trigger event configuration of line 11
      TR11           : FTSR_TR11_Field := 16#0#;
      --  Falling trigger event configuration of line 12
      TR12           : FTSR_TR12_Field := 16#0#;
      --  Falling trigger event configuration of line 13
      TR13           : FTSR_TR13_Field := 16#0#;
      --  Falling trigger event configuration of line 14
      TR14           : FTSR_TR14_Field := 16#0#;
      --  Falling trigger event configuration of line 15
      TR15           : FTSR_TR15_Field := 16#0#;
      --  Falling trigger event configuration of line 16
      TR16           : FTSR_TR16_Field := 16#0#;
      --  Falling trigger event configuration of line 17
      TR17           : FTSR_TR17_Field := 16#0#;
      --  unspecified
      Reserved_18_18 : stm32f030.Bit := 16#0#;
      --  Falling trigger event configuration of line 19
      TR19           : FTSR_TR19_Field := 16#0#;
      --  unspecified
      Reserved_20_31 : stm32f030.UInt12 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for FTSR_Register use record
      TR0            at 0 range 0 .. 0;
      TR1            at 0 range 1 .. 1;
      TR2            at 0 range 2 .. 2;
      TR3            at 0 range 3 .. 3;
      TR4            at 0 range 4 .. 4;
      TR5            at 0 range 5 .. 5;
      TR6            at 0 range 6 .. 6;
      TR7            at 0 range 7 .. 7;
      TR8            at 0 range 8 .. 8;
      TR9            at 0 range 9 .. 9;
      TR10           at 0 range 10 .. 10;
      TR11           at 0 range 11 .. 11;
      TR12           at 0 range 12 .. 12;
      TR13           at 0 range 13 .. 13;
      TR14           at 0 range 14 .. 14;
      TR15           at 0 range 15 .. 15;
      TR16           at 0 range 16 .. 16;
      TR17           at 0 range 17 .. 17;
      Reserved_18_18 at 0 range 18 .. 18;
      TR19           at 0 range 19 .. 19;
      Reserved_20_31 at 0 range 20 .. 31;
   end record;

   subtype SWIER_SWIER0_Field is stm32f030.Bit;
   subtype SWIER_SWIER1_Field is stm32f030.Bit;
   subtype SWIER_SWIER2_Field is stm32f030.Bit;
   subtype SWIER_SWIER3_Field is stm32f030.Bit;
   subtype SWIER_SWIER4_Field is stm32f030.Bit;
   subtype SWIER_SWIER5_Field is stm32f030.Bit;
   subtype SWIER_SWIER6_Field is stm32f030.Bit;
   subtype SWIER_SWIER7_Field is stm32f030.Bit;
   subtype SWIER_SWIER8_Field is stm32f030.Bit;
   subtype SWIER_SWIER9_Field is stm32f030.Bit;
   subtype SWIER_SWIER10_Field is stm32f030.Bit;
   subtype SWIER_SWIER11_Field is stm32f030.Bit;
   subtype SWIER_SWIER12_Field is stm32f030.Bit;
   subtype SWIER_SWIER13_Field is stm32f030.Bit;
   subtype SWIER_SWIER14_Field is stm32f030.Bit;
   subtype SWIER_SWIER15_Field is stm32f030.Bit;
   subtype SWIER_SWIER16_Field is stm32f030.Bit;
   subtype SWIER_SWIER17_Field is stm32f030.Bit;
   subtype SWIER_SWIER19_Field is stm32f030.Bit;

   --  Software interrupt event register (EXTI_SWIER)
   type SWIER_Register is record
      --  Software Interrupt on line 0
      SWIER0         : SWIER_SWIER0_Field := 16#0#;
      --  Software Interrupt on line 1
      SWIER1         : SWIER_SWIER1_Field := 16#0#;
      --  Software Interrupt on line 2
      SWIER2         : SWIER_SWIER2_Field := 16#0#;
      --  Software Interrupt on line 3
      SWIER3         : SWIER_SWIER3_Field := 16#0#;
      --  Software Interrupt on line 4
      SWIER4         : SWIER_SWIER4_Field := 16#0#;
      --  Software Interrupt on line 5
      SWIER5         : SWIER_SWIER5_Field := 16#0#;
      --  Software Interrupt on line 6
      SWIER6         : SWIER_SWIER6_Field := 16#0#;
      --  Software Interrupt on line 7
      SWIER7         : SWIER_SWIER7_Field := 16#0#;
      --  Software Interrupt on line 8
      SWIER8         : SWIER_SWIER8_Field := 16#0#;
      --  Software Interrupt on line 9
      SWIER9         : SWIER_SWIER9_Field := 16#0#;
      --  Software Interrupt on line 10
      SWIER10        : SWIER_SWIER10_Field := 16#0#;
      --  Software Interrupt on line 11
      SWIER11        : SWIER_SWIER11_Field := 16#0#;
      --  Software Interrupt on line 12
      SWIER12        : SWIER_SWIER12_Field := 16#0#;
      --  Software Interrupt on line 13
      SWIER13        : SWIER_SWIER13_Field := 16#0#;
      --  Software Interrupt on line 14
      SWIER14        : SWIER_SWIER14_Field := 16#0#;
      --  Software Interrupt on line 15
      SWIER15        : SWIER_SWIER15_Field := 16#0#;
      --  Software Interrupt on line 16
      SWIER16        : SWIER_SWIER16_Field := 16#0#;
      --  Software Interrupt on line 17
      SWIER17        : SWIER_SWIER17_Field := 16#0#;
      --  unspecified
      Reserved_18_18 : stm32f030.Bit := 16#0#;
      --  Software Interrupt on line 19
      SWIER19        : SWIER_SWIER19_Field := 16#0#;
      --  unspecified
      Reserved_20_31 : stm32f030.UInt12 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for SWIER_Register use record
      SWIER0         at 0 range 0 .. 0;
      SWIER1         at 0 range 1 .. 1;
      SWIER2         at 0 range 2 .. 2;
      SWIER3         at 0 range 3 .. 3;
      SWIER4         at 0 range 4 .. 4;
      SWIER5         at 0 range 5 .. 5;
      SWIER6         at 0 range 6 .. 6;
      SWIER7         at 0 range 7 .. 7;
      SWIER8         at 0 range 8 .. 8;
      SWIER9         at 0 range 9 .. 9;
      SWIER10        at 0 range 10 .. 10;
      SWIER11        at 0 range 11 .. 11;
      SWIER12        at 0 range 12 .. 12;
      SWIER13        at 0 range 13 .. 13;
      SWIER14        at 0 range 14 .. 14;
      SWIER15        at 0 range 15 .. 15;
      SWIER16        at 0 range 16 .. 16;
      SWIER17        at 0 range 17 .. 17;
      Reserved_18_18 at 0 range 18 .. 18;
      SWIER19        at 0 range 19 .. 19;
      Reserved_20_31 at 0 range 20 .. 31;
   end record;

   subtype PR_PR0_Field is stm32f030.Bit;
   subtype PR_PR1_Field is stm32f030.Bit;
   subtype PR_PR2_Field is stm32f030.Bit;
   subtype PR_PR3_Field is stm32f030.Bit;
   subtype PR_PR4_Field is stm32f030.Bit;
   subtype PR_PR5_Field is stm32f030.Bit;
   subtype PR_PR6_Field is stm32f030.Bit;
   subtype PR_PR7_Field is stm32f030.Bit;
   subtype PR_PR8_Field is stm32f030.Bit;
   subtype PR_PR9_Field is stm32f030.Bit;
   subtype PR_PR10_Field is stm32f030.Bit;
   subtype PR_PR11_Field is stm32f030.Bit;
   subtype PR_PR12_Field is stm32f030.Bit;
   subtype PR_PR13_Field is stm32f030.Bit;
   subtype PR_PR14_Field is stm32f030.Bit;
   subtype PR_PR15_Field is stm32f030.Bit;
   subtype PR_PR16_Field is stm32f030.Bit;
   subtype PR_PR17_Field is stm32f030.Bit;
   subtype PR_PR19_Field is stm32f030.Bit;

   --  Pending register (EXTI_PR)
   type PR_Register is record
      --  Pending bit 0
      PR0            : PR_PR0_Field := 16#0#;
      --  Pending bit 1
      PR1            : PR_PR1_Field := 16#0#;
      --  Pending bit 2
      PR2            : PR_PR2_Field := 16#0#;
      --  Pending bit 3
      PR3            : PR_PR3_Field := 16#0#;
      --  Pending bit 4
      PR4            : PR_PR4_Field := 16#0#;
      --  Pending bit 5
      PR5            : PR_PR5_Field := 16#0#;
      --  Pending bit 6
      PR6            : PR_PR6_Field := 16#0#;
      --  Pending bit 7
      PR7            : PR_PR7_Field := 16#0#;
      --  Pending bit 8
      PR8            : PR_PR8_Field := 16#0#;
      --  Pending bit 9
      PR9            : PR_PR9_Field := 16#0#;
      --  Pending bit 10
      PR10           : PR_PR10_Field := 16#0#;
      --  Pending bit 11
      PR11           : PR_PR11_Field := 16#0#;
      --  Pending bit 12
      PR12           : PR_PR12_Field := 16#0#;
      --  Pending bit 13
      PR13           : PR_PR13_Field := 16#0#;
      --  Pending bit 14
      PR14           : PR_PR14_Field := 16#0#;
      --  Pending bit 15
      PR15           : PR_PR15_Field := 16#0#;
      --  Pending bit 16
      PR16           : PR_PR16_Field := 16#0#;
      --  Pending bit 17
      PR17           : PR_PR17_Field := 16#0#;
      --  unspecified
      Reserved_18_18 : stm32f030.Bit := 16#0#;
      --  Pending bit 19
      PR19           : PR_PR19_Field := 16#0#;
      --  unspecified
      Reserved_20_31 : stm32f030.UInt12 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for PR_Register use record
      PR0            at 0 range 0 .. 0;
      PR1            at 0 range 1 .. 1;
      PR2            at 0 range 2 .. 2;
      PR3            at 0 range 3 .. 3;
      PR4            at 0 range 4 .. 4;
      PR5            at 0 range 5 .. 5;
      PR6            at 0 range 6 .. 6;
      PR7            at 0 range 7 .. 7;
      PR8            at 0 range 8 .. 8;
      PR9            at 0 range 9 .. 9;
      PR10           at 0 range 10 .. 10;
      PR11           at 0 range 11 .. 11;
      PR12           at 0 range 12 .. 12;
      PR13           at 0 range 13 .. 13;
      PR14           at 0 range 14 .. 14;
      PR15           at 0 range 15 .. 15;
      PR16           at 0 range 16 .. 16;
      PR17           at 0 range 17 .. 17;
      Reserved_18_18 at 0 range 18 .. 18;
      PR19           at 0 range 19 .. 19;
      Reserved_20_31 at 0 range 20 .. 31;
   end record;

   -----------------
   -- Peripherals --
   -----------------

   --  External interrupt/event controller
   type EXTI_Peripheral is record
      --  Interrupt mask register (EXTI_IMR)
      IMR0   : aliased IMR_Register;
      --  Event mask register (EXTI_EMR)
      EMR0   : aliased EMR_Register;
      --  Rising Trigger selection register (EXTI_RTSR)
      RTSR0  : aliased RTSR_Register;
      --  Falling Trigger selection register (EXTI_FTSR)
      FTSR0  : aliased FTSR_Register;
      --  Software interrupt event register (EXTI_SWIER)
      SWIER0 : aliased SWIER_Register;
      --  Pending register (EXTI_PR)
      PR0    : aliased PR_Register;
   end record
     with Volatile;

   for EXTI_Peripheral use record
      IMR0   at 16#0# range 0 .. 31;
      EMR0   at 16#4# range 0 .. 31;
      RTSR0  at 16#8# range 0 .. 31;
      FTSR0  at 16#C# range 0 .. 31;
      SWIER0 at 16#10# range 0 .. 31;
      PR0    at 16#14# range 0 .. 31;
   end record;

   --  External interrupt/event controller
   EXTI_Periph : aliased EXTI_Peripheral
     with Import, Address => EXTI_Base;

end stm32f030.EXTI;
