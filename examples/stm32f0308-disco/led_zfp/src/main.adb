with Last_Chance_Handler;
with led_periodic;
with System.Machine_Code;

procedure Main is
   use System.Machine_Code;
begin
   led_periodic.led_periodic_init;
   led_periodic.timer6_init;
   led_periodic.usart4_init;
   led_periodic.ADC_Temp_Init;
   Asm("cpsie i" & ASCII.LF & ASCII.HT, Volatile => True);

   loop
      led_periodic.led_periodic_task;
      led_periodic.usart4_task;

      declare
         delay_counter : Integer := 1000000 with Volatile;
      begin
         while delay_counter > 0 loop
            delay_counter := delay_counter - 1;
            --led_periodic.timer6_task;
         end loop;
      end;
   end loop;
end Main;
