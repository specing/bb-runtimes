package Ring_Buffer is
   type RingBuffer is limited private;
   procedure Initialize(RB: in out RingBuffer);
   procedure Put(RB: in out RingBuffer; C : in Character);
   function Get(RB: in out RingBuffer) return Character;
   function Is_Empty(RB: in RingBuffer) return Boolean;
   function Is_Full(RB: in RingBuffer) return Boolean;
private
   size : constant := 256;
   type Index is mod size;
   type Data_Array is array (Index'Range) of Character;
   type RingBuffer is
      record
         Head : Index;
         Tail : Index;
         Data : Data_Array;
      end record;
end;
