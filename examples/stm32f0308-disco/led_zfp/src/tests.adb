with Ring_Buffer;
with Ada.Text_IO; use Ada.Text_IO;


procedure Tests is
   use all type Ring_Buffer.RingBuffer;

   RB : Ring_Buffer.RingBuffer;

   Str : constant String := "Hello world";



begin
   Initialize(RB);
   for i in Str'Range loop
      if Is_Full(RB) then
         Put_Line("RB is full!");
      else
         Put(RB, Str(I));
      end if;
   end loop;

   Put("Contents of RB: ");

   while not Is_Empty(RB) loop
      Put(Get(RB));
      exit;
   end loop;

   Put(RB, '.');

   while not Is_Empty(RB) loop
      Put(Get(RB));
   end loop;
   New_Line;


   declare
      type Data_Type is mod 2**12;
      Data : Data_Type := 100;
      T : Data_Type;

      S : String(1..10);
      I : Integer := 1;
   begin
      while Data > 0 loop
         T := Data mod 16;
         if T < 10 then
            S(I) := Character'Val(Character'Pos('0') + T);
         else
            T := T - 10;
            S(I) := Character'Val(Character'Pos('A') + T);
         end if;
         I := I + 1;
         Data := Data / 16;
      end loop;
      -- Now unroll the string back
      for Ind in reverse 1 .. I-1 loop
         Put(S(Ind));
      end loop;

      New_Line;
   end;
end;
