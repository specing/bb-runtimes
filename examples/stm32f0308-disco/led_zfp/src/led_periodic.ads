package led_periodic is

   procedure led_periodic_init;
   procedure led_periodic_task;
   procedure timer6_init;
   procedure timer6_task;
   procedure usart4_init;
   procedure usart4_task;
   procedure ADC_Temp_Init;

end led_periodic;
