--with Ada.Interrupts.Names;

with stm32f072.GPIO;
with stm32f072.RCC;
with stm32f072.TIM;
with stm32f072.USART;
with stm32f072.NVIC;
with stm32f072.ADC;

with Ring_Buffer;

with System.Machine_Code;
pragma Warnings(Off);
with Interfaces.STM32; use Interfaces.STM32;
pragma Warnings(On);

package body led_periodic is
   use all type stm32f072.Bit;

   procedure Enable_Interrupts
     with Inline is
   begin
      System.Machine_Code.Asm("cpsie i" & ASCII.LF & ASCII.HT, Volatile => True);
   end;

   procedure Disable_Interrupts
     with Inline is
   begin
      System.Machine_Code.Asm("cpsid i" & ASCII.LF & ASCII.HT, Volatile => True);
   end;


   U4 : stm32f072.USART.USART_Peripheral renames stm32f072.USART.USART4_Periph;
   U4RB : Ring_Buffer.RingBuffer;
   procedure U4_Put(C: Character) is
      use all type Ring_Buffer.RingBuffer;
   begin
      -- If transmision is in progress then we just add another character
      -- to the FIFO else we have to add the character and trigger the usart interrupt
      Disable_Interrupts;
      if Is_Empty(U4RB) then
         U4.CR1.TXEIE := 1; -- will hopefully trigger an interrupt right
         -- when ints are enabled
      end if;
      Put(U4RB, C);
      Enable_Interrupts;
   end;

   procedure USART34_ISR
     with Export, Convention => C, External_Name => "___USART34_ISR";
--   pragma Attach_Handler(USART34_ISR, 17);
   procedure USART34_ISR is
      use all type Ring_Buffer.RingBuffer;
   begin
      if U4.ISR.TXE = 1 then
         Disable_Interrupts;
         if Is_Empty(U4RB) then
            U4.CR1.TXEIE := 0;
         else
            U4.TDR.TDR := stm32f072.Uint9(Character'Pos(Get(U4RB)));
         end if;
         Enable_Interrupts;
      end if;
   end;



   procedure U4_Put(S : String) is
   begin
      for C of S loop
         U4_Put(C);
      end loop;
   end;
   procedure U4_New_Line is
   begin
      U4_Put(ASCII.CR);
      U4_Put(ASCII.LF);
   end;
   procedure U4_Put_Line(S : String) is
   begin
      U4_Put(S);
      U4_New_Line;
   end;

   procedure U4_Put(Data : Integer'Base) is
      type NotSigned is mod (Integer'Base'Last + 1) * 2;
      Temp : NotSigned;
      Digit : NotSigned;

      S : String(1..12);
      I : Integer := 1;
   begin
      if Data < 0 then
         U4_Put('-');
         Temp := NotSigned(-Data);
      else
         Temp := NotSIgned(Data);
      end if;

      while Temp > 0 loop
         Digit := Temp mod 16;
         if Digit < 10 then
            S(I) := Character'Val(Character'Pos('0') + Digit);
         else
            Digit:= Digit - 10;
            S(I) := Character'Val(Character'Pos('A') + Digit);
         end if;
         I := I + 1;
         Temp := Temp / 16;
      end loop;
      -- Now unroll the string back
      for Ind in reverse 1 .. I-1 loop
         U4_Put(S(Ind));
      end loop;
   end;








   procedure ADC_Temp_Trigger;





   type t_led_state is (on, off);
   type Led_Rotation_t is Mod 4;
   Led_Rotation : Led_Rotation_t;

   GPIOC : stm32f072.GPIO.GPIO_Peripheral renames stm32f072.GPIO.GPIOC_Periph;

   procedure Led_Periodic_Init is
   begin
      --  stm32f072rb discovery has 4 leds of different colours connected on
      --  PC6(red), PC7(blue), PC8(orange), PC9(green)
      --  Initialization is as follows:
      --     Enable GPIOC clock in RCC
      stm32f072.RCC.RCC_Periph.AHBENR.IOPCEN := 1;
      --     Configure all led pins to function as high-speed push-pull GPOs
      GPIOC.MODER.Arr(6..9)   := (others => 2#01#); -- gen purp out
      --  Configure LD3 (PC09) as General purpose output mode
      --  Configure LD3 (PC09) as High speed
      --  Configure LD3 (PC09) as Pull-up
--      stm32f072.GPIO.GPIOC_Periph.OSPEEDR0.OSPEEDR9 := 2#11#;
--      stm32f072.GPIO.GPIOC_Periph.PUPDR0.PUPDR9     := 2#10#;
      --RCC_AHBENR    := RCC_AHBENR    or 16#8_0000#;
      --GPIOC_MODER   := GPIOC_MODER   or 16#4_0000#;
      --GPIOC_OSPEEDR := GPIOC_OSPEEDR or 16#C_0000#;
      --GPIOC_PUPDR   := GPIOC_PUPDR   or 16#8_0000#;
      Led_Rotation := 0;
   end Led_Periodic_Init;

   procedure Led_Periodic_Task is
   begin
      GPIOC.BSRR.BR.Arr := (6 => 1, 7 => 1, 9 => 1, others => 0);
      --stm32f072.GPIO.GPIOC_Periph.BSRR0.BR8 := 1;
      case Led_Rotation is
         when 0 =>
            GPIOC.BSRR.BS.Arr(6) := 1;
         when 1 =>
            GPIOC.BSRR.BS.Arr(7) := 1;
         when 2 =>
            null;
            --stm32f072.GPIO.GPIOC_Periph.BSRR0.BS8 := 1;
         when 3 =>
            GPIOC.BSRR.BS.Arr(9) := 1;
      end case;
      Led_Rotation := Led_Rotation + 1;
   end Led_Periodic_Task;

   led8_state : t_led_state;
   TIM6 : stm32f072.TIM.TIM6_Peripheral renames stm32f072.TIM.TIM6_Periph;
   NVIC : stm32f072.NVIC.NVIC_Peripheral renames stm32f072.NVIC.NVIC_Periph;
   procedure Timer6_init is
   begin
--      stm32f072.RCC.RCC_Periph.APB1ENR0.TIM6EN := 1;
      stm32f072.RCC.RCC_Periph.APB1ENR.TIM6EN := 1;
      led8_state := off;
      --  Fire once per second
      TIM6.PSC.PSC := 733;
      TIM6.ARR.ARR := 65484;

      -- interrupts
      TIM6.DIER := (UIE => 1, UDE => 0, others => <>);
      -- Enable interrupt in NVIC
      NVIC.ISER := 2#0000_0000_0000_0010_0000_0000_0000_0000#;

      TIM6.CR1.CEN := 1;
   end;

   procedure Timer6_task is
   begin
      if TIM6.SR.UIF = 1 then
         case led8_state is
            when off =>
               GPIOC.BSRR.BS.Arr(8) := 1;
               led8_state := on;
            when on =>
               GPIOC.BSRR.BR.Arr(8) := 1;
               led8_state := off;
         end case;
         TIM6.SR.UIF := 0;
      end if;
      U4_Put('T');
      U4_Put('I');
      U4_Put('M');
      U4_Put('6');
      U4_Put(ASCII.CR);
      U4_Put(ASCII.LF);
      ADC_Temp_Trigger;
   end;

--   procedure TIM6_ISR;
--   pragma Export (C, TIM6_ISR, "___TIM6_ISR");

   procedure TIM6_ISR
     with Export, Convention => C, External_Name => "___TIM6_ISR";
   procedure TIM6_ISR is
   begin
      --GPIOC.BSRR.BS.Arr(8) := 1;
      case led8_state is
         when off =>
            GPIOC.BSRR.BS.Arr(8) := 1;
            led8_state := on;
         when on =>
            GPIOC.BSRR.BR.Arr(8) := 1;
            led8_state := off;
      end case;
      TIM6.SR := (UIF => 0, others => 0);
   end;









   procedure USART4_Init is
   begin
      stm32f072.RCC.RCC_Periph.APB1ENR.USART4EN := 1;
      -- USART4 is alternate function 0 .. AF0
      --  GPIOC.AFRH.Arr(2) :=
      GPIOC.MODER.Arr(10 .. 11) := (others => 2#10#); -- alternate function mode
      GPIOC.OSPEEDR.Arr(10 .. 11) := (others => 2#11#); -- High speed
      --  DIV_Fraction   at 0 range 0 .. 3;
      --  DIV_Mantissa   at 0 range 4 .. 15;
      -- 16#1a1# for 115200 baud at 48 MHz = 2#0001_1010_0001#
      U4.BRR := (DIV_Fraction => 2#0001#, DIV_Mantissa => 2#0001_1010#, others => 0);

      NVIC.ISER := 2#0010_0000_0000_0000_0000_0000_0000_0000#;
      --U4.CR1.RXNEIE := 1;
      --U4.CR1 := (UE => 1, TE => 1, RE => 1, others => <>); -- +200 bytes WTF?
      U4.CR1.UE := 1;
      U4.CR1.TE := 1;
      --U4.CR1.RE := 1;

      Ring_Buffer.Initialize(U4RB);
      U4_Put_Line("Boot");
   end;

   procedure USART4_Task is
   begin
      null;--U4.TDR.TDR := 16#41#; -- yell AAAAAAAA
   end;





   ADC : stm32f072.ADC.ADC_Peripheral renames stm32f072.ADC.ADC_Periph;
   procedure ADC_Temp_Init is
   begin
      -- Enable peripheral clock (watch out, there are two clock sources)
      stm32f072.RCC.RCC_Periph.APB2ENR.ADCEN := 1;
      ADC.CFGR2.JITOFF_D.Val := 2#01#; -- PCLK/2 sync mode

      -----------------
      --  Calibrate ADC  (RM0091r9p937)
      --    ADEN will be clear at reset
      --    DMAEN will be clear at reset
      --
      ADC.CR.ADCAL := 1;
      loop
         exit when ADC.CR.ADCAL = 0;
         -- have to wait for 4 ADC clock cycles after ADCAL is set to 0?
         -- RM0091r9 page 232
      end loop;

      ----------------
      -- Turn ON ADC
      --   RM0091 page 231  ADC turn on/off
      --   ADRDY should be 0 at reset
      if ADC.ISR.ADRDY = 1 then
         ADC.ISR.ADRDY := 1; -- clear it
      end if;
      ADC.CR.ADEN := 1;
      loop
         exit when ADC.ISR.ADRDY = 1;
      end loop;
      ADC.CR.ADEN := 1;
      loop
         exit when ADC.ISR.ADRDY = 1;
      end loop;

      -- Enable interrupts
      NVIC.ISER := 2#0000_0000_0000_0000_0001_0000_0000_0000#;
      ADC.IER.EOCIE := 1; -- End Of Conversion

      -- Reading the temperature, RM0091r9p251
      ADC.CCR.TSEN := 1;
      -- highest sampling time, should be foolproof
      -- Datasheet says 4 micro seconds for f072rb
      ADC.SMPR.SMPR := 2#111#; -- 10us when using 48MHz PCLK/2
      ADC.CHSELR.CHSEL.Arr(16) := 1;
   end;

   procedure ADC_Temp_Trigger is
   begin
      ADC.CR.ADSTART := 1;
   end;

   procedure ADC_Temp_ISR
     with Export, Convention => C, External_Name => "___ADC_COMP_ISR";
   procedure ADC_Temp_ISR is
   begin
      if ADC.ISR.EOC = 1 then
         declare
            type Data_Type is mod 2**12;
            --Data : stm32f072.UInt16 := ADC.DR.DATA;
            --Temp : stm32f072.UInt16;
            Data : Data_Type := Data_Type(ADC.DR.DATA);
            Temp : Data_Type;

            S : String(1..3);
            I : Integer := 1;
         begin
            while Data > 0 loop
               Temp := Data mod 16;
               if Temp < 10 then
                  S(I) := Character'Val(Character'Pos('0') + Temp);
               else
                  Temp := Temp - 10;
                  S(I) := Character'Val(Character'Pos('A') + Temp);
               end if;
               I := I + 1;
               Data := Data / 16;
            end loop;
            -- Now unroll the string back
            for Ind in reverse 1 .. I-1 loop
               U4_Put(S(Ind));
            end loop;

            U4_Put(ASCII.CR);
            U4_Put(ASCII.LF);
         end;
      else
         U4_Put('?');
      end if;
   end;
end led_periodic;
